import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from 'axios'

Vue.config.productionTip = false

Vue.prototype.$eventBus = new Vue()

const httpInstance = axios.create({
  baseURL: process.env.VUE_APP_CHIRPI_API_BASE_URL
});

Vue.prototype.$chirpihttp = httpInstance

httpInstance.interceptors.request.use(config => {
  Vue.prototype.$eventBus.$emit('loading', true)
  return config
})

httpInstance.interceptors.response.use(function (response) {
  Vue.prototype.$eventBus.$emit('loading', false)
  Vue.prototype.$eventBus.$emit('message-http-response', response)
  return response
}, function (error) {
  Vue.prototype.$eventBus.$emit('loading', false)
  Vue.prototype.$eventBus.$emit('message-http-error', error)
  return error
})

Vue.prototype.$chirpisettings = {
	port: '',
	radio: ''
}

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
