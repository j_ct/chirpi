const GoogleFontsPlugin = require("@beyonk/google-fonts-webpack-plugin")

module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    configureWebpack: {
        plugins: [
            new GoogleFontsPlugin({
                fonts: [
                    { family: "Roboto", variants: [ "100","300", "400", "500", "700", "900" ] }
                ]
            })
        ]
    }
}