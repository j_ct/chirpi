# ChirPi Development

This project consists of three main components:

- A series of bash scripts used to install dependencies and configure the Raspberry Pi.
- An API (Python/Flask) which interacts with the radio via USB and manages Chirp mmap files.
- A webapp (JavaScript/Vue.js) which serves as the user interface and communicates with the API.


The following sections will hopefully help get you started working on these. These notes assume that you've followed the steps above to setup up a Raspberry Pi with ChirPi installed.

### Scripts

The scripts are located in `scripts`. 

What each does should be fairly straight forward based on the name.

Currently the best way to work on them then just running them on a Pi.

### API / Backend

The API code is located in `api`. It is a Flask app the relies heavily on Flask-RESTful for creating resource endpoints.

#### Running Locally

Although the API can be run locally for development, I choose to run it on the Pi to avoid cross platform weirdness with Chirp.

If you do decide to run the API locally, you'll probably want to create a virtual environment before installing dependencies in `api/requirements.txt` using pip.

#### Running on the Pi

On the Pi, due to compilation issues and slowness, Python packages are not installed using virtual a environment. The install script handles running pip install.

##### Watch Deploy Script

To make it easier to make changes locally and upload them to the Pi, there is a script that watches for changes and rsynces them automatically.

On Linux you may need to install inotify-tools for this to work. With apt:

```
sudo apt-get install inotify-tools
```

To run the script:

```
./tools/dev-watch-api.sh
```

This script will only watch for changes in the `api` directory, but will rsync the entire project to the Pi.

#### Starting the API

To start the API Flask app run:

```
python api/main.py
```

You can also run the command that systemctl does:

```
cd /opt/chirpi/api
authbind /home/pi/.local/bin/gunicorn -b 0.0.0.0:80 -w 1 main:app
```

### App / Frontend

The code for the webapp is located in `app`. It is a poorly written Vue.js app.

#### Running Locally

When developing the webapp, running it locally is highly recommended for speed and because it's just easier. The app can still communicate with the API running on a Pi over the network.

To run the app locally you'll need Node > v10 installed on your machine as well as NPM.

To install the app's dependencies, run:

```
cd app
npm install
```

#### Starting the App

To start the app, in `/app` run:

```
npm run serve
```

#### Other Commands

These other npm commands are also available in the `/app` directory:

##### Compiles and minifies for production

```
npm run build
```

##### Lints and fixes files

```
npm run lint
```