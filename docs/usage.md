# Using ChirPi

## Getting Connected

1. Connect a USB OTG cable to the other USB port on the Raspberry Pi and plug the programming cable into the USB OTG Cable.

2. Connect a micro USB cable to a suitable power supply and the power port of the Raspberry Pi.

3. After 20-30 seconds a WiFi network called **chirpi** should be visible, connect to it from a phone like you would any other WiFi network. (The default pass phrase is **radioisfun**).

Note: Your phone may complain that this network is not connected to the Internet (because it is not). If it asks you, select the option to connect anyway.

4. Open **http://chirpi.local** in your phone's web browser (don't leave off the **http://** or you might end up somewhere else).

## Loading Images 

You'll need to have one or more radio configuration files or "images" on the ChirPi device. These files are the type used by Chirp on a desktop computer.

There are two ways to add an image file to ChirPi:

1) Select "Upload Image From Local Storage" to upload an image file that is saved to your phone's storage. (Tip: If you have these on a computer, you can access ChirPi from your computer and upload them directly.)

2) Select "Download Image From Radio" to create a new image file by downloading data from a radio connected via the programming cable.

## Working With Images

Once you've loaded an image to ChirPi, tap or click its name to see available options.

Currently, you can rename or delete images, view information about saved channels, and upload the image to a radio.

### Uploading to Radio

With your radio programming cable plugged in and the radio turned on, select an image then select "Upload to Radio".

You will be prompted for the port the radio is plugged into and radio model number. These fields work the same way as they do in the Chirp desktop app.

Lastly, tap the "Upload to Radio" button to begin the upload.