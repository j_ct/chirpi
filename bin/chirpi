#!/usr/bin/env bash
#
# Borrowed heavily from the wonderful oh-my-zsh install script:
# https://github.com/ohmyzsh/ohmyzsh/blob/master/tools/install.sh
#
# This script should be run via curl:
#   curl -fsSL https://gitlab.com/j_ct/chirpi/-/raw/master/bin/chirpi | bash -s -- install
#
# As an alternative, you can first download the install script and run it afterwards:
#   wget https://gitlab.com/j_ct/chirpi/-/raw/master/tools/install.sh
#   bash ./chripi install
#
# Respects the following environment variables:
#   REPO    - name of the GitHub repo to install from (default: j_ct/chirpi)
#   REMOTE  - full remote URL of the git repo to install (default: GitHub via HTTPS)
#   BRANCH  - branch to check out immediately after install (default: master)
#
REPO=${REPO:-j_ct/chirpi}
REMOTE=${REMOTE:-https://gitlab.com/${REPO}.git}
BRANCH=${BRANCH:-master}
INSTALL_DIR=${chirpi_install_dir:-'/opt/chirpi'}
CHIRPI_USER=${chirpi_user:-'pi'}

command_exists() {
	command -v "$@" >/dev/null 2>&1
}

fmt_error() {
  echo ${RED}"Error: $@"${RESET} >&2
}

fmt_underline() {
  echo "$(printf '\033[4m')$@$(printf '\033[24m')"
}

fmt_code() {
  echo "\`$(printf '\033[38;5;247m')$@${RESET}\`"
}

setup_color() {
	# Only use colors if connected to a terminal
	if [ -t 1 ]; then
		RED=$(printf '\033[31m')
		GREEN=$(printf '\033[32m')
		YELLOW=$(printf '\033[33m')
		BLUE=$(printf '\033[34m')
		BOLD=$(printf '\033[1m')
		RESET=$(printf '\033[m')
	else
		RED=""
		GREEN=""
		YELLOW=""
		BLUE=""
		BOLD=""
		RESET=""
	fi
}

apt_update() {
	echo "${GREEN}Updating packages...${RESET}"
	sudo apt-get update
}

install_git() {
	command_exists git || {
    	echo "${GREEN}Installing git...${RESET}"
		sudo apt-get install git -y
  	}
}

make_install_dir() {
	echo "${GREEN}Creating install dir ${INSTALL_DIR}...${RESET}"
	sudo mkdir -p $INSTALL_DIR
	sudo chown -R $CHIRPI_USER:$CHIRPI_USER $INSTALL_DIR
}

download_chirpi() {
	umask g-w,o-w

  	echo "${GREEN}Cloning/updating ChirPi...${RESET}"

  	command_exists git || {
    	fmt_error "git is not installed"
    	exit 1
  	}

  	if [[ -d "${INSTALL_DIR}/.git" ]]
  	then
  		cd "$INSTALL_DIR"
		git pull "$REMOTE" "$BRANCH"
  	else
  		git clone \
    	--depth=1 --branch "$BRANCH" "$REMOTE" "$INSTALL_DIR" || {
	    	fmt_error "git clone of ChirPi repo failed"
	    	exit 1
	  	}
	fi
}

load_config() {
	echo "${GREEN}Loading config...${RESET}"

	config_filename="config.ini"

	config_base_path="${INSTALL_DIR}/${config_filename}"

	if [ -e "${config_base_path}" ]
	then
	    source "${config_base_path}"
	    echo "${GREEN}Loaded config file \"${config_base_path}\"${RESET}"
	else
	    fmt_error "ERROR: Can't find the base config file \"${config_base_path}\""
	    exit 1
	fi

	if [ -e "${chirpi_user_config_path}" ]
	then
	    echo "${GREEN}Loaded config file \"${chirpi_user_config_path}\"${RESET}"
	else
	    echo "${YELLOW}Can't find the user config file \"${chirpi_user_config_path}\", skipping${RESET}"
	fi

	variables=(
	    chirpi_install_dir
	    chirpi_user_dir
	    chirpi_hostname
	    chirpi_tld
	    #chirpi_eth0_static_ip
	    chirpi_chirp_install_dir
	    chirpi_chirp_repo_url
	    chirpi_chirp_repo_branch
	    chirpi_wlan_if
	    chirpi_wlan_static_ip
	    chirpi_wlan_static_ip_cidr
		chirpi_wlan_dhcp_range
		chirpi_wlan_channel
		chirpi_wlan_ssid
		chirpi_wlan_wpa_passphrase
		chirpi_mmaps_dir
		chirpi_user_config_path
		chirpi_nameserver
	)

	for variable in "${variables[@]}"
	do
	    if [[ -z ${!variable+x} ]]; then   # indirect expansion here
	        fmt_error "ERROR: The variable \"${variable}\" is missing from your \""${settings_file}"\" file.";
	        exit 2
	    fi
	done
}

set_hostname() {
	echo "${GREEN}Updating hostname... ${RESET}"
	echo $chirpi_hostname | sudo tee /etc/hostname > /dev/null
	sudo sed -i "s/raspberrypi/$chirpi_hostname/g" /etc/hosts
	sudo hostname $chirpi_hostname
}

set_static_ip() {
	echo "${GREEN}Setting static IP on eth0... ${RESET}"

	echo "# define static profile
profile static_eth0
static ip_address=${chirpi_eth0_static_ip}

# fallback to static profile on eth0
interface eth0
fallback static_eth0" | sudo tee /etc/dhcpcd.conf > /dev/null

}

install_chirp() {
	echo "${GREEN}Installing Chirp dependencies... ${RESET}"
	sudo apt-get install python-gtk2 \
	python-libxml2 python-libxslt1 python-serial \
	python-support python-future -y
	# sudo apt-get install libxml2-dev libxslt-dev -y
	# only seems necessary when attempting to compile 
	# lxml pip package, but pi zero is too slow for that

	echo "${GREEN}Creating Chirp directory... ${RESET}"
	sudo mkdir -p $chirpi_chirp_install_dir
	sudo chown -R $CHIRPI_USER:$CHIRPI_USER $chirpi_chirp_install_dir

	if [[ -d "${chirpi_chirp_install_dir}/.git" ]]
	then
		echo "${GREEN}Pulling Chirp repository... ${RESET}"
		cd "$chirpi_chirp_install_dir"
		git pull "$chirpi_chirp_repo_url" "$chirpi_chirp_repo_branch"
	else
		echo "${GREEN}Cloning Chirp repository... ${RESET}"
		git clone \
	    	--depth=1 --branch "$chirpi_chirp_repo_branch" "$chirpi_chirp_repo_url" "$chirpi_chirp_install_dir" || {
	    	fmt_error "git clone of Chirp repo failed"
	    	exit 1
	  	}
	fi

	echo "${GREEN}Making chirpc executable... ${RESET}"
	chmod +x $chirpi_chirp_install_dir/chirpc

	echo "${GREEN}Adding user to dailout group... ${RESET}"
	sudo addgroup $CHIRPI_USER dialout
}

setup_access_point() {
	echo "${GREEN}Installing access point dependencies... ${RESET}"
	sudo apt-get install hostapd dnsmasq -y

	echo "${GREEN}Stopping access point services... ${RESET}"
	sudo systemctl stop hostapd
	sudo systemctl stop dnsmasq

	echo "${GREEN}Updating access point config files... ${RESET}"
	if [ -f "/etc/dhcpcd.conf.orig" ]; then
	    sudo cp /etc/dhcpcd.conf.orig /etc/dhcpcd.conf
	fi
	sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.orig
	sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.ap

	echo "interface wlan0
static ip_address=${chirpi_wlan_static_ip}/${chirpi_wlan_static_ip_cidr}
nohook wpa_supplicant" | sudo tee -a /etc/dhcpcd.conf.ap > /dev/null

	if [ -f "/etc/dnsmasq.conf.orig" ]; then
	    sudo cp /etc/dnsmasq.conf.orig /etc/dnsmasq.conf
	fi
	sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
	sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.ap

	echo "interface=${chirpi_wlan_if}
dhcp-range=${chirpi_wlan_dhcp_range},24h
domain=local
address=/${chirpi_hostname}.${chirpi_tld}/${chirpi_wlan_static_ip}
expand-hosts" | sudo tee /etc/dnsmasq.conf.ap > /dev/null

	echo "interface=${chirpi_wlan_if}
hw_mode=g
channel=${chirpi_wlan_channel}
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
ssid=${chirpi_wlan_ssid}
wpa_passphrase=${chirpi_wlan_wpa_passphrase}" | sudo tee /etc/hostapd/hostapd.conf > /dev/null

	if [ -f "/etc/default/hostapd.orig" ]; then
	    sudo cp /etc/default/hostapd.orig /etc/default/hostapd
	fi
	sudo cp /etc/default/hostapd /etc/default/hostapd.orig
	sudo cp /etc/default/hostapd /etc/default/hostapd.ap

	echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' | sudo tee -a /etc/default/hostapd.ap > /dev/null

	echo "${chirpi_wlan_static_ip}     ${chirpi_hostname}.${chirpi_tld}" | sudo tee -a /etc/hosts > /dev/null
	enable_access_point

}

make_mmap_dir() {
	echo "${GREEN}Creating user mmap dir... ${RESET}"
	sudo mkdir -p $chirpi_mmaps_dir
	sudo chown -R $CHIRPI_USER:$CHIRPI_USER $chirpi_mmaps_dir
	sudo chown -R $CHIRPI_USER:$CHIRPI_USER $chirpi_user_dir
}

copy_user_config() {
	[[ -f $chirpi_user_config_path ]] || {
		echo "${GREEN}Copying user config file... ${RESET}"
		sudo cp "${INSTALL_DIR}/config.user-template.ini" "$chirpi_user_config_path"
		sudo chown -R $CHIRPI_USER:$CHIRPI_USER $chirpi_user_config_path
	}
}

install_chirpi_pip_reqs() {
	command_exists pip || {
    	echo "${GREEN}Installing pip...${RESET}"
		sudo apt-get install python-pip -y
  	}
  	echo "${GREEN}Installing pip requirements...${RESET}"
	pip install -r "${INSTALL_DIR}/api/requirements.txt"
}

enable_access_point() {
	echo "${GREEN}Move enable access point configs into place...${RESET}"
	sudo cp /etc/dhcpcd.conf.ap /etc/dhcpcd.conf
	sudo cp /etc/dnsmasq.conf.ap /etc/dnsmasq.conf
	sudo cp /etc/default/hostapd.ap /etc/default/hostapd

	echo "${GREEN}Enable access point services... ${RESET}"
	sudo systemctl unmask hostapd.service
	sudo systemctl unmask dnsmasq.service
	sudo systemctl enable hostapd.service
	sudo systemctl enable dnsmasq.service
}

start_access_point() {
	echo "${GREEN}Start access point services... ${RESET}"
	echo "${YELLOW}Warning: You will likely lose connectivity after these commands are executed.${RESET}"
	echo "${YELLOW}To reconnect, manually reboot the Pi after a few seconds, then connect to its AP network.${RESET}"
	sudo systemctl start hostapd.service
	sudo systemctl start dnsmasq.service
}

disable_access_point() {
	echo "${GREEN}Move disable access point configs into place...${RESET}"
	sudo cp /etc/dhcpcd.conf.orig /etc/dhcpcd.conf
	sudo cp /etc/dnsmasq.conf.orig /etc/dnsmasq.conf
	sudo cp /etc/default/hostapd.orig /etc/default/hostapd

	echo "${GREEN}Disable access point services... ${RESET}"
	sudo systemctl disable dnsmasq.service
	sudo systemctl disable hostapd.service
}

stop_access_point() {
	echo "${GREEN}Stop access point services... ${RESET}"
	echo "${YELLOW}Warning: You will likely lose connectivity after these commands are executed.${RESET}"
	echo "${YELLOW}To reconnect, manually reboot the Pi after a few seconds, then find it on the network defined in wpa_supplicant.conf${RESET}"
	sudo systemctl stop dnsmasq.service
	sudo systemctl stop hostapd.service
}


setup_api_service() {
	echo "${GREEN}Setting up API system service... ${RESET}"

	gunicorn_path="/home/${CHIRPI_USER}/.local/bin/gunicorn"

	command_exists "$gunicorn_path" || {
    	echo "${GREEN}Installing gunicorn...${RESET}"
		pip install gunicorn
  	}

  	command_exists "$gunicorn_path" || {
    	fmt_error "Could not install gunicorn or command does not exist."
    	exit 1
  	}

  	#Allow access to port 80
  	sudo apt-get install authbind -y
  	sudo touch /etc/authbind/byport/80
	sudo chmod 500 /etc/authbind/byport/80
	sudo chown $CHIRPI_USER /etc/authbind/byport/80

	echo "[Unit]
Description=ChirPi Api
After=network.target

[Service]
User=${CHIRPI_USER}
WorkingDirectory=${INSTALL_DIR}/api
ExecStart=authbind ${gunicorn_path} -b 0.0.0.0:80 -w 1 main:app
Restart=always

[Install]
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/chirpi.service > /dev/null
	
	 sudo systemctl daemon-reload
	 sudo systemctl enable chirpi
}

show_access_point_info() {
	echo ""
	echo "${BOLD}You are about to be prompted to restart the device.${RESET}"
	echo "${BOLD}When it turns back on, you should be able to connect to \
its Wi-Fi network - ${RESET}"
	echo "${BOLD}SSID: ${chirpi_wlan_ssid}${RESET}"
	echo "${BOLD}Passphrase: ${chirpi_wlan_wpa_passphrase}${RESET}"
}

prompt_restart() {
	echo ""
	echo "${BOLD}Restart required!"
	read -n 1 -s -r -p "Press any key to initiate restart."
	sudo reboot
}

link_bin() {
	sudo ln -s "${INSTALL_DIR}/bin/chirpi" /usr/local/bin/chirpi
}

sub_install() {

	apt_update
	install_git
	make_install_dir
	download_chirpi

	load_config
	make_mmap_dir
	copy_user_config

	install_chirpi_pip_reqs
	setup_api_service
	install_chirp
	link_bin

	set_hostname
	#set_static_ip
	setup_access_point
	show_access_point_info
	prompt_restart
}

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    install   Install chirPi"
    echo "    apoff   Disable and stop wi-fi access point and connect to network in wpa_supplicant.conf"
    echo "    apon   Enable and start wi-fi access point"
    echo ""
}
  
sub_apon(){
    echo "Enabling standalone access point."
    enable_access_point
    start_access_point
}

sub_apoff(){
    echo "Disabling standalone access point."
    disable_access_point
    stop_access_point
}

setup_color
	
echo "${BOLD}ChirPi Install/Config Tool${RESET}"

ProgName=$(basename $0)

subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac

