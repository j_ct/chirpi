import serial
import os
import sys
import argparse
import logging
import time
import werkzeug
import timeago
import datetime
import re

from stat import S_ISREG, ST_CTIME, ST_MODE
from dotenv import load_dotenv

config_filename="config.ini"
base_config_path=os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    os.pardir,
    config_filename)

app_dist_path=os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    os.pardir,
    'app',
    'dist')

load_dotenv(dotenv_path=base_config_path)
load_dotenv(os.getenv('chirpi_user_config_path'))

sys.path.insert(1, os.getenv('chirpi_chirp_install_dir'))

from chirp import logger
from chirp.drivers import *
from chirp import chirp_common, errors, directory, util, platform

LOG = logging.getLogger("chirpc")
RADIOS = directory.DRV_TO_RADIO

from flask import Flask, send_from_directory
from flask_restful import Resource, Api, reqparse, abort
from flask_cors import CORS

app = Flask(__name__, static_folder=app_dist_path)
CORS(app, origins="*")
api = Api(app)

parser = reqparse.RequestParser()

mem_props = [
    'comment',
    'cross_mode',
    'ctone',
    'dtcs',
    'dtcs_polarity',
    'duplex',
    'extd_number',
    'freq',
    'immutable',
    'mode',
    'name',
    'number',
    'offset',
    'power',
    'rtone',
    'rx_dtcs',
    'skip',
    'tmode',
    'tuning_step',
    'vfo',
    'x']


def file_name_string(value):
    if value == '':
        raise ValueError("File name cannot be empty")
    if " " in value:
        raise ValueError("File name cannot contain spaces")
    if not value.endswith('.img'):
        raise ValueError("File name must end in .img")
    if not re.match(r"^[\w\-\._~:\(\)\+.]+$", value):
        raise ValueError("File name contains invalid special characters")

    return value


class SupportedRadios(Resource):
    def get(self):
        return sorted(RADIOS.keys())


class Ports(Resource):
    def get(self):
        ports = platform.get_platform().list_serial_ports()
        return ports


class RadioDetect(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('port', required=True,
help="port cannot be blank!")
        args = parser.parse_args()
        from chirp import detect
        md = detect.detect_icom_radio(args['port'])
        return md.MODEL


class Mmaps(Resource):
    def get(self):
        dirpath = os.getenv('chirpi_mmaps_dir')

        entries = (os.path.join(dirpath, fn) for fn in os.listdir(dirpath))
        entries = ((os.stat(path), path) for path in entries)
        entries = ((stat[ST_CTIME], path)
                   for stat, path in entries if S_ISREG(stat[ST_MODE]))
        result = []
        now = datetime.datetime.now()
        for cdate, path in sorted(entries, reverse=True):
            if path.endswith('.img'):
                ago = timeago.format(cdate, now)
                result.append({
                    'name': os.path.basename(path),
                    'ago': ago})
        return result
    
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument('file', required=True, help="File required.",
         type=werkzeug.datastructures.FileStorage, location='files')
        parse.add_argument('name', required=True, type=file_name_string)
        args = parse.parse_args()
        file = args['file']
        path = os.path.join(os.getenv('chirpi_mmaps_dir'), args['name'])
        if(os.path.exists(path)):
            abort(400, message="File named %s already exists, please use a different name." % args['name'])
        app.logger.info(path)
        file.stream.seek(0)
        file.save(path)
        if(os.path.exists(path)):
            return {'name': args['name'], 'message': 'Successfully saved %s.' % args['name']}
        else:
            abort(500, message="Could not save file, please try again.")


class RadioDownload(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('port', required=True,
            help="port required")
        parser.add_argument('radio', required=True,
            help="radio required")
        parser.add_argument('name', required=True,
            help="File name required")
        args = parser.parse_args()

        path = os.path.join(os.getenv('chirpi_mmaps_dir'), args['name'])
        
        if(os.path.exists(path)):
            abort(400, message="File named %s already exists, please use a different name." % args['name'])

        rclass = directory.get_radio(args['radio'])
        
        app.logger.info("opening %s at %i" % (args['port'], rclass.BAUD_RATE))
        
        s = serial.Serial(port=args['port'],
                              baudrate=rclass.BAUD_RATE,
                              timeout=0.5)
        radio = rclass(s)
        
        if not issubclass(rclass, chirp_common.CloneModeRadio):
            errmessage = "%s is not a clone mode radio" % args['radio']
            app.logger.error(errmessage)
            abort(400, message=errmessage)
        try:
            radio.sync_in()
            radio.save_mmap(path)
            return {'message': 'Successfully downloaded from radio and saved as %s' % args['name']}
        except Exception, e:
            app.logger.error(e)
            abort(500, message="Unable to download from radio, please ensure that it is connected and turned on.")


class BaseMmap(Resource):
    def _get_path(self, file):
        return os.path.join(os.getenv('chirpi_mmaps_dir'), str(file).strip())


class Mmap(BaseMmap):
    def get(self, file):
        mmap_path = self._get_path(file)
        result = {
            'memories': [],
            'settings': {},
        }
        r = directory.get_radio_by_image(mmap_path)
        result['radio'] = '%s_%s' % (r.VENDOR, r.MODEL)
        rclass = r.__class__
        radio = rclass(mmap_path)

        settings = radio.get_settings()
        for i in range(0, len(settings)):
            group_name = str(settings[i].get_name())
            items_clean = {}
            for k, v in settings[i].items():
                items_clean[k] = str(v)
            result['settings'][group_name] = items_clean

        rf = radio.get_features()
        start, end = rf.memory_bounds
        for i in range(start, end + 1):
            mem = radio.get_memory(i)
            if mem.empty:
                continue
            mem_result = {}
            for prop in mem_props:
                mem_result[prop] = str(getattr(mem,prop))
                mem_result['format_freq'] = mem.format_freq()
            result['memories'].append(mem_result)
        return result

    def delete(self, file):
        mmap_path = self._get_path(file)
        if os.path.exists(mmap_path):
            os.remove(mmap_path)
            app.logger.info("Deleted %s" % mmap_path)
            return {'message': "Deleted %s." % file}
        else:
            abort(400, message="Could not delete %s. File does not exist." % file)


    def put(self, file):
        parser = reqparse.RequestParser()
        parser.add_argument('newname')
        args = parser.parse_args()

        mmap_path = self._get_path(file)
        if not os.path.exists(mmap_path):
            abort(400, message="Could not update %s, file does not exist." % file)
            return
        if args['newname'] and not args['newname'] == '' and not file == args['newname']:
            new_mmap_path = self._get_path(args['newname'])
            from shutil import copy2
            try:
                copy2(mmap_path, new_mmap_path)
            except Exception, e:
                abort(400, message="Unable to rename %s." % file)
            
            os.remove(mmap_path)
            app.logger.info("Renamed %s" % mmap_path)
            return {'message': "Renamed %s to %s." % (file, args['newname'])}
        else:
            return {'message': "No changes detected."}
            

class MmapUpload(BaseMmap):
    def post(self, file):
        mmap_path = self._get_path(file)

        parser = reqparse.RequestParser()
        parser.add_argument('port', required=True,
            help="port required")
        parser.add_argument('radio', required=True,
            help="radio required")
        args = parser.parse_args()

        rclass = directory.get_radio(args['radio'])
        
        app.logger.info("opening %s at %i" % (args['port'], rclass.BAUD_RATE))

        s = serial.Serial(port=args['port'],
                              baudrate=rclass.BAUD_RATE,
                              timeout=0.5)

        radio = rclass(s)

        if not issubclass(rclass, chirp_common.CloneModeRadio):
            errmessage = "%s is not a clone mode radio" % args['radio']
            app.logger.error(errmessage)
            abort(400, message=errmessage)
        try:
            radio.load_mmap(mmap_path)
            radio.sync_out()
            app.logger.info("Upload successful")
            return {'message': 'Successfully uploaded %s' % args['name']}
        except Exception, e:
            app.logger.error(e)
            abort(500, message="Unable to upload to radio, please ensure that it is connected and turned on.")

        return args


@app.route('/')
def serve_index():
    return send_from_directory(app_dist_path, 'index.html')


@app.route('/<path:path>')
def serve_page(path):
    return send_from_directory(app_dist_path, path)

api.add_resource(SupportedRadios, '/radios')
api.add_resource(Ports, '/ports')
api.add_resource(RadioDetect, '/radio/detect')
api.add_resource(RadioDownload, '/radio/download')
api.add_resource(Mmaps, '/mmaps')
api.add_resource(Mmap, '/mmap/<file>')
api.add_resource(MmapUpload, '/mmap/<file>/upload')


if __name__ == '__main__':
    app.run(debug=bool(os.getenv('chirpi_api_debug')), host=os.getenv('chirpi_api_host'))

