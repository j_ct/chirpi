#!/usr/bin/env bash

# Watch a directory and deploy to Pi using rsync when
# changes are detected. You may need to install 
# inotify-tools for this to work:
# sudo apt-get install inotify-tools

current_dir=$(dirname "${BASH_SOURCE[0]}")
watch_string=$(realpath "${current_dir}/../api")
deploy_dir="${current_dir}/../"
ssh_string="pi@chirpi.local"
deploy_dest="/opt/chirpi"

while inotifywait -e modify "${watch_string}"; do
 	rsync -av --exclude="*/node_modules/*" --exclude=".git/*" "${deploy_dir}" "${ssh_string}:${deploy_dest}"
done