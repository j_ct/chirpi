# ChirPi

![Logo](./docs/logo.png)

Easily program radios on the go with configuration files from the popular Chirp software using just a Raspberry Pi and smart phone. 

<img src="./docs/app-screenshot.png" width="500">

## Overview

### Features

- Upload Chirp image files from connected devices to Pi.
- Program supported radios using uploaded image files.
- Download configuration from radio and save as image file.
- View image file settings and memories from connected devices.
- Rename, duplicate and delete image files.

### How it works

1. The Pi creates a WiFi hotspot that can be joined from a phone or other device.
2. Connected devices access a web application that is hosted on the Pi.
3. The web app interfaces with an API that is also hosted on the Pi.
4. The API uses Chirp's python library to interface with a radio that is plugged into the Pi.

### Disclaimers

This software is currently under development and will have bugs.

It should be considered very insecure and should only be run on hosts that are not connected to the internet.

## Creating a ChirPi

### Gather Materials

| Item | Notes |
| ---- | ----- |
| Raspberry Pi Zero W | This is the preferred and tested model, due to its size and built in WiFi capabilities and is small, but other models might work. |
| Micro SD card | Should be at least 8GB, class 10 preferred. See [RaspberryPi Documentation - SD cards](https://www.raspberrypi.org/documentation/installation/sd-cards.md) |
| Power supply + micro USB cable | Should put out at least 1.2A for Pi Zero W. See [RaspberryPi Documentation - Power Supply](https://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md) |
| Micro USB OTG cable | This is a cable that is a USB A female connector to micro USB male connector, which is required to plug typical USB devices into the Pi Zero |
| Radio programming cable | Discussion of what the best one is can be found elsewhere, but one that normally works with Chirp will work for this project. |
| (Optional) USB Ethernet Dongle | Useful for development if you need to connect directly to the Pi without Wi-Fi. |

### Prepare Raspberry Pi

#### Prepare SD Card

1. Download the latest version of the [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/) and [write it to an SD card](https://www.raspberrypi.org/documentation/installation/installing-images/).

2. Place a text file on the boot partition named `ssh` to enable ssh.

3. Create a `wpa_supplicant.conf` file on the boot partition to connect to a WiFi network. Example file:

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
     ssid="Your network name/SSID"
     psk="Your WPA/WPA2 security key"
     key_mgmt=WPA-PSK
}
```

#### First Boot

Log into the Pi via SSH:

```
ssh pi@raspberrypi.local
```

(default password is raspberry)

Ensure that your Pi is connected to the internet:

```
ping gitlab.com
```

If not, you can run `sudo raspi-config` to edit the Wi-Fi connection details.

Perform other typical first boot tasks, such as

- Upgrading software packages:

```
sudo apt-get update
sudo apt-get upgrade -y
```

- Changing the default password:

```
passwd
```

- Adding your SSH key to `authorized_keys`. You can use something like `ssh-copy-id` or [here's a tutorial](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md).

**Before going forward, you should have a Raspberry Pi that you are able to SSH into and that is connected to the Internet.**

### Run Installer

Log in to your Pi and run the following command:

```
curl -fsSL https://gitlab.com/j_ct/chirpi/-/raw/master/bin/chirpi | bash -s -- install
```

This will download the cli tool `bin/chirpi` and then run the install command. This does a lot of things, and hopefully it works for you.

## Using ChirPi

If the installation script ran without errors, at the end you'll see a message with an SSID and passphrase for a Wi-Fi network that now allows you to interface with ChirPi. The default is chirpi / radioisfun.

You can connect to the network from any device and go to [http://chirpi.local](http://chirpi.local) to access the app.

For more detailed usage instructions, see [./docs/usage.md](./docs/usage.md)

### Troubleshooting

In my experience, not seeing the Wi-Fi network, or the Pi not recognizing the serial port of the programmig cable, and similar issues, can sometimes by resolved by just power cycling the Pi a few times. Especially when attempting to plugin in the new USB peripherals while the Pi is running.

### Connecting To The Internet

By default ChirPi is configured as an access point - it creates its own wifi network that you can connect to. For updates or for development, you might need to connect ChirPi to a wifi network to access to The Internet.

Before connecting to another wifi network, you'll need to update `/etc/wpa_supplicant.conf` with the information for that network.

To disable access point mode log in via SSH and run:

```
chirpi apoff
```

Then reboot the Pi. 

#### Restoring Access Point Mode

To go back to access point mode log in via SSH and run:

```
chirpi apon
```

Then reboot the Pi.

### Development

See [./docs/development.md](./docs/development.md).